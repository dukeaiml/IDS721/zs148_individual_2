# IDS721 Individual Project 2

## Author
Ziyu Shi

## Web Service
This individual project is based on the work of my week 2 mini-project. A new feature has been added to this project. The web service receives an input string by parsing the query parameter text. Then, it iterates over this string, calculates the occurrence count of each character, and returns these frequencies in JSON format. If the text parameter is not provided in the request, it will return a 400 Bad Request response.

## Demo video
The demo video of the entire process of the web service is in the root directory of my GitLab repo. It displays the process of starting a docker container wrapping the service, testing on the website, testing with `curl` command and stopping the docker container.

## Screenshots
### New feature code
![image](images/newFunction.png)

### Build docker image
![image](images/buildImage.png)

### Check new image
![image](images/showImage.png)

### Run docker container on 8080 port
![image](images/dockerRun.png)

### Get hostname to access the web service
![image](images/hostname.png)

### Website
![image](images/website.png)

### Test on website
![image](images/websiteTest.png)

### Test by curl
![image](images/curlTest.png)


## Install Docker
Use .sh script provided by Docker offical website to automatically install Docker.
```
curl -fsSL https://test.docker.com -o test-docker.sh
sudo sh get-docker.sh
```

## Verify Docker
Use the following command to test whether Docker has been installed well on your system.
```
sudo docker run hello-world
```
If the terminal appeals this means that Docker has been well installed.
```
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
b8dfde127a29: Pull complete
Digest: sha256:308866a43596e83578c7dfa15e27a73011bdd402185a84c5cd7f32a88b501a24
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
```

## Create a Rust web application
1. Create a template by
    ```
    cargo new <PROJECT_NAME>
    ```
2. Add needed dependencies to `Cargo.toml`
3. Write content in the `src/main.rs`
4. Verify the application locally by `cargo run`

## Write Dockerfile
My Dockerfile:
```dockerfile
# Use the right version correponding to your rust version
FROM rust:latest AS builder

# set up work directory
WORKDIR /myapp
USER root

# copy the entire project into the working dicrectory
COPY . .

# compile rust app in the working directory
RUN cargo build --release

# use the right image according to different versions of glibc
FROM debian:bookworm-slim

# set up working directory
WORKDIR /myapp

# copy the executable file to the working directory for easily launching
COPY --from=builder /myapp/target/release/actix_web_app /myapp

# expose port
EXPOSE 8080

# run the app
CMD ["./actix_web_app"]
```

## Build the Docker image and run container
### Build your Docker image by
```
sudo docker build -t <YOUR_IMAGE> .
```

### Run the container locally by
```
sudo docker run -p 8080:8080 <YOUR_IMAGE>
```
Then you can check your web application if running.

To reuse an existing Docker container, use:
```
sudo docker start <CONTAINER_ID>
```

To stop a running Docker container, use:
```
sudo docker stop <CONTAINER_ID>
```

## To test my web application
After run the container locally, use the following command to get your hostname:
```
hostname -I
```
### Test method 1
Use `<HOST_NAME>:8080` as URL to directly access the website and test by add the following suffix to URL
   ```
   /char_freq?text=<TEST_CHAR>
   ```
### Test method 2
Use `curl` to test in terminal and get the result in JSON format
   ```
   curl <HOST_NAME>:8080/char_freq?text=<TEST_CHAR>
   ```

## Build GitLab CI/CD pipelines
Create `.gitlab-ci.yml` file. Use the `dind` service to run a docker daemon inside and so allow the CI/CD task running in the GitLab Runner's Docker to execute Docker operation. Create a docker image and run the container in the background. Here is my GitLab CI/CD file:
```yaml
image: docker:25.0.3

variables:
  IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  DOCKER_HOST: tcp://docker:2375
  DOCKER_DRIVER: overlay2
  DOCKER_TLS_CERTDIR: ""

services:
  - docker:dind

stages:
  - test

test:
  stage: test
  before_script:
  - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
  script:
    - docker build -t ids-individual-2 .
    - docker run -d --name ind-2-container -p 8080:8080 ids-individual-2
    - docker ps -a
```
