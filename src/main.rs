use actix_web::{get, web, App, HttpResponse, HttpServer, Responder};
use std::collections::HashMap;
use serde_json::to_string;

#[get("/")]
async fn index() -> impl Responder {
    "Welcome! Use /char_freq?text=yourtext to calculate character frequencies."
}

// calculate the occurance of characters
#[get("/char_freq")]
async fn char_freq(web::Query(info): web::Query<HashMap<String, String>>) -> impl Responder {
    match info.get("text") {
        Some(text) => {
            let mut freqs: HashMap<char, usize> = HashMap::new();
            for c in text.chars() {
                *freqs.entry(c).or_insert(0) += 1;
            }
            match to_string(&freqs) {
                Ok(json_str) => {
                    HttpResponse::Ok().body(format!("{}\n", json_str))
                },
                Err(e) => HttpResponse::InternalServerError().body(format!("Error: {}", e))
            }
        }
        None => HttpResponse::BadRequest().body("Missing text query parameter"),
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .service(index)
            .service(char_freq)
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
